import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Form_2 from '../../components/apply/form_2';

class Start extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'start')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const backgroundImage = { backgroundImage: "url(" + this.state.doc.data.image.url + ")" };
            return (
                <div style={backgroundImage} className="background-cover horizontal-center">
                    <div className="width-pc-553 max-width-px-655 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                        <h3 className="color-738 margin-bottom-lh-361">{RichText.asText(this.state.doc.data.title)}</h3>
                        <Form_2/>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Start;
