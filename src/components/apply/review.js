import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import '../../static/css/review.css';

class Review extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'review')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const backgroundImage = { backgroundImage: "url(" + this.state.doc.data.image.url + ")" };
            return (
                <div id="review-apply" className="background-color-738">
                    <div style={backgroundImage} id="review-apply-1" className="background-cover min-height-px-155">

                    </div>
                    <div id="review-apply-2" className="vertical-center padding-left-166 padding-right-166 padding-bottom-361 padding-top-361">
                        <div>
                            <h4 className="color-492 margin-bottom-lh-361">{RichText.asText(this.state.doc.data.review)}</h4>
                            <p className="color-366">{RichText.asText(this.state.doc.data.barber)}</p>
                        </div>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Review;
