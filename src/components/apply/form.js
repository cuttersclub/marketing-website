import React, {Component} from 'react';
import $ from 'jquery';

class Form extends Component {

    componentDidMount = () => {
        // POST form data to zapier on submit
        $('#myForm').submit(function(e){
            let name = $('#name').val()
            let email = $('#email').val()
            let url = 'https://calendly.com/cuttersclub/interview?name=' + name + '&email=' + email;
            e.preventDefault();
            $.ajax({
                url:'https://hooks.zapier.com/hooks/catch/2736235/n7i6ms/',
                type:'post',
                data:$('#myForm').serialize(),
                success:function(){
                  // Redirect to another success page
                  window.location = url;
                }
            });
        });
    };

    render() {
        return (
            <div>
                <form id="myForm" className="grid background-color-738 max-width-px-655 width-pc-553 border-radius-149 padding-top-428 padding-bottom-428">
                    <input id="name" required type="text" name="name" placeholder="Name" className="color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input id="email" required type="email" name="email" placeholder="Email" className="color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input required type="text" name="phone" placeholder="Phone" className="color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input required type="text" name="postcode" placeholder="Postcode" className="color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <button id="Form-submit" type="submit" className="background-color-492 color-738 height-px-361 border-radius-149 margin-left-428 margin-right-428 pointer">Apply now</button>
                </form>
            </div>
        );
    }
}

export default Form;
