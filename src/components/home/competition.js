import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Form_2 from '../home/form_2';

class Competition extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'competition')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const backgroundImage = { backgroundImage: "url(" + this.state.doc.data.image.url + ")" };
            return (
                <div style={backgroundImage} className="background-cover vertical-center horizontal-center">
                    <div className="max-width-px-655 width-pc-553 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">

                        <h3 className="color-738 margin-bottom-lh-361">{RichText.asText(this.state.doc.data.title)}</h3>

                        <p className="color-738 margin-bottom-lh-428">{RichText.asText(this.state.doc.data.description_1)}</p>
                        <p className="color-738 margin-bottom-lh-428">{RichText.asText(this.state.doc.data.description_2)}</p>
                        <p className="color-738 margin-bottom-lh-361">{RichText.asText(this.state.doc.data.description_3)}</p>

                        <Form_2/>

                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Competition;
