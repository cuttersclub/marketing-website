import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import $ from 'jquery';

import Logo from '../static/img/logo.svg'
import Arrow from '../static/img/arrow.svg'

class Header extends Component {

    render() {

        $(document).ready(function(){
            $("#mobile-logo").on("click", function(){
                $("#mobile-logo").toggleClass("open");
                $("#mobile-menu").toggleClass("open");
                $("#arrow").toggleClass("open");
                $("#notification-bar").toggleClass("open");
                $("#body").toggleClass("open");
            })
        });

        return (
          <header className='background-color-492 width-pc-553'>
            <a id='desktop-logo' href='/' className='absolute'>
              <img id='logo' className='margin-left-166' src={Logo}/>
            </a>
            <a id='mobile-logo' className='fixed'>
              <img id='logo' className='margin-left-166' src={Logo}/>
              <img id="arrow" className="width-px-428 margin-left-428 margin-top-699 margin-bottom-699" src={Arrow}/>
            </a>
            <Link to={{pathname: '/apply'}} id="desktop-menu-option" className="float-right color-738 margin-left-166 margin-right-166">Become a barber</Link>

            <div id="mobile-menu" className="fixed height-px-721 overflow-hidden background-color-492 vertical-center horizontal-center width-pc-553">
                <div>
                    <a href='/' className="color-738">Home</a><br/>
                    <a href='/apply' className="color-738">Become a barber</a><br/>
                </div>
            </div>

          </header>
        )
    }
}

export default Header;
