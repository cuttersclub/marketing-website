import React, {Component} from 'react';
import $ from 'jquery';

class Form_2 extends Component {

    render() {

        var executed = false;
        function appendText() {
            if (executed == false) {
                executed = true;
                var txt2 = $("<button id='button-2' name='upviralsubmit' type='submit' class='background-color-492 color-738 height-px-361 border-radius-149 margin-left-428 margin-right-428'></button>").text("Find your barber");   // Create with jQuery
                $("#form-2").append(txt2);      // Append the new elements
                var x = document.getElementById("button-2");
                    x.style.display = "none";
            }
        };

        return (
            <div>
                <form name='upviralForm64223' id="form-2" method='post' className="grid background-color-738 max-width-px-655 width-pc-553 border-radius-149 padding-top-428 padding-bottom-428" action='https://app.upviral.com/site/parse_new_users/call/ajax/campId/64223'>
                    <input onInput={() => appendText()} required placeholder="Name" type='text' name='name' id="name-2" className="form-control color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input required placeholder="Email" type='text' name='email' id="email-2" className="form-control color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input required placeholder="Postcode" type='text' name='Postcode' id="postcode-2" className="form-control color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <div id="button-2" className="background-color-492 color-738 height-px-361 border-radius-149 margin-left-428 margin-right-428 vertical-center horizontal-center pointer"><p>Find your barber</p></div>
                </form>
            </div>
        );
    }
}

export default Form_2;
