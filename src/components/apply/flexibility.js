import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import '../../static/css/flexibility.css';

class Flexibility extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'flexibility')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            return (
                <div className="background-color-738 horizontal-center">
                    <div id="choose-when-you-work-center" className="max-width-px-257 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                        <div id="choose-when-you-work-center-1" className="vertical-center horizontal-center">
                            <div>
                                <h3 className="color-492 margin-bottom-lh-361">{RichText.asText(this.state.doc.data.title)}</h3>
                                <p className="color-366">{RichText.asText(this.state.doc.data.description)}</p>
                            </div>
                        </div>
                        <div id="choose-when-you-work-center-2" className="vertical-center horizontal-center">
                            <img className="height-px-357 shadow border-radius-149" src={this.state.doc.data.image.url}></img>
                        </div>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Flexibility;
