import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import '../../static/css/reviews.css';

class Reviews extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'reviews')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            return (
                <div className="background-color-191 horizontal-center">
                    <div className="max-width-px-257 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                        <h3 className="color-492 margin-bottom-lh-421">{RichText.asText(this.state.doc.data.title)}</h3>
                        <div id="reviews-grid">
                            <div id="reviews-grid-1" className="vertical-center horizontal-center margin-bottom-361">
                                <div>
                                    <div className="background-color-738 shadow border-radius-149 padding-left-166 padding-right-166 padding-top-361 padding-bottom-361 margin-bottom-166">
                                        <h5 className="color-492">{RichText.asText(this.state.doc.data.review_1)}</h5>
                                    </div>
                                    <img className="height-px-361 border-radius-895" src={this.state.doc.data.image_1.url}></img>
                                    <p className="color-366 margin-top-lh-428">{RichText.asText(this.state.doc.data.customer_1)}</p>
                                </div>
                            </div>
                            <div id="reviews-grid-2" className="vertical-center horizontal-center margin-bottom-361">
                                <div>
                                    <div className="background-color-738 shadow border-radius-149 padding-left-166 padding-right-166 padding-top-361 padding-bottom-361 margin-bottom-166">
                                        <h5 className="color-492">{RichText.asText(this.state.doc.data.review_2)}</h5>
                                    </div>
                                    <img className="height-px-361 border-radius-895" src={this.state.doc.data.image_2.url}></img>
                                    <p className="color-366 margin-top-lh-428">{RichText.asText(this.state.doc.data.customer_1)}</p>
                                </div>
                            </div>
                            <div id="reviews-grid-3" className="vertical-center horizontal-center">
                                <div>
                                    <div className="background-color-738 shadow border-radius-149 padding-left-166 padding-right-166 padding-top-361 padding-bottom-361 margin-bottom-166">
                                        <h5 className="color-492">{RichText.asText(this.state.doc.data.review_3)}</h5>
                                    </div>
                                    <img className="height-px-361 border-radius-895" src={this.state.doc.data.image_3.url}></img>
                                    <p className="color-366 margin-top-lh-428">{RichText.asText(this.state.doc.data.customer_1)}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Reviews;
