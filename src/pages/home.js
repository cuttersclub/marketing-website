import React, {Component} from 'react';
import MetaTags from 'react-meta-tags';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Header from '../components/header';
import Viewport from '../components/home/viewport';
import Footer from '../components/footer';
import Form from '../components/home/form';
import How_it_works from '../components/home/how_it_works';
import Reviews from '../components/home/reviews';
import Competition from '../components/home/competition';
import '../static/css/home.css';

class Home extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'home')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            return (
                <div>
                <MetaTags>
                    <title> {RichText.asText(this.state.doc.data.title_tag)} </title>
                    <meta name="description" content={RichText.asText(this.state.doc.data.meta_description)} />
                </MetaTags>
                    <Header/>
                    <Viewport/>
                    <How_it_works/>
                    <Reviews/>
                    <Competition/>
                    <Footer/>
                </div>
            )
        }
        return <h1></h1>;
    }
}

export default Home;
