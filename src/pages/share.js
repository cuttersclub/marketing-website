import React, {Component} from 'react';
import MetaTags from 'react-meta-tags';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Header from '../components/header';
import Viewport from '../components/share/viewport';
import Footer from '../components/footer';
import '../static/css/home.css';

class Share extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'share')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            return (
                <div>
                    <MetaTags>
                        <title> {RichText.asText(this.state.doc.data.title_tag)} </title>
                        <meta name="description" content={RichText.asText(this.state.doc.data.meta_description)} />
                        <meta name="robots" content="noindex"/>
                    </MetaTags>
                    <Header/>
                    <Viewport/>
                    <Footer/>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Share;
