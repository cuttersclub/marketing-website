import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import '../../static/css/upviral.css';

class Upviral extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount () {
        const me = this;
        window.UpviralConfig = {
            camp: "MA(AA$",
            widget_style:'iframe',
            width:"100%"
        };
        me.loadScript('https://snippet.upviral.com/upviral.js')
            .then(() => {})
            .catch((error) => {
                console.log('Error when loading upviral: ', error);
            });
    }


    // this function will work cross-browser for loading scripts asynchronously
    loadScript(src) {
        return new Promise(function(resolve, reject) {
            const s = document.createElement('script');
            let r = false;
            s.type = 'text/javascript';
            s.src = src;
            s.async = true;
            s.onerror = function(err) {
                reject(err, s);
            };
            s.onload = s.onreadystatechange = function() {
                // console.log(this.readyState); // uncomment this line to see which ready states are called.
                if (!r && (!this.readyState || this.readyState == 'complete')) {
                    r = true;
                    resolve();
                }
            };
            const t = document.getElementsByTagName('script')[0];
            t.parentElement.insertBefore(s, t);
        });
    }

    render() {
        return (
            <React.Fragment>
                <div id="iframe_wrapper" className="overflow-hidden border-radius-149">
                    <iframe className="uvembed64223" frameBorder="0" src="https://static.upviral.com/loader.html"></iframe>
                </div>
            </React.Fragment>
        );
    }
}

export default Upviral;
