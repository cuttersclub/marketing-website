import React, {Component} from 'react';
import $ from 'jquery';

class Form extends Component {

    render() {

        var executed = false;
        function appendText() {
            if (executed == false) {
                executed = true;
                var txt2 = $("<button id='button' name='upviralsubmit' type='submit' class='background-color-492 color-738 height-px-361 border-radius-149 margin-left-428 margin-right-428'></button>").text("Find your barber");   // Create with jQuery
                $("#form").append(txt2);      // Append the new elements
                var x = document.getElementById("button");
                    x.style.display = "none";
            }
        };

        return (
            <div>
                <form name='upviralForm64223' id="form" method='post' className="grid background-color-738 max-width-px-655 width-pc-553 border-radius-149 padding-top-428 padding-bottom-428" action='https://app.upviral.com/site/parse_new_users/call/ajax/campId/64223'>
                    <input onInput={() => appendText()} required placeholder="Name" type='text' name='name' id="name" className="form-control color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input required placeholder="Email" type='text' name='email' id="email" className="form-control color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <input required placeholder="Postcode" type='text' name='Postcode' id="postcode" className="form-control color-366 height-px-361 margin-left-428 margin-right-428"></input>
                    <div id="button" className="background-color-492 color-738 height-px-361 border-radius-149 margin-left-428 margin-right-428 vertical-center horizontal-center pointer"><p>Find your barber</p></div>
                </form>
            </div>
        );
    }
}

export default Form;
