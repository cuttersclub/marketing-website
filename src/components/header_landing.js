import React, {Component} from 'react';
import Logo from '../static/img/logo.svg'

class Header extends Component {
    render() {
        return (
          <header className='background-color-492 width-pc-553'>
            <div className='absolute'>
              <img id='logo' className='margin-left-166' src={Logo}/>
            </div>
          </header>
        )
    }
}

export default Header;
