import React, {Component} from 'react';
import MetaTags from 'react-meta-tags';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Header from '../components/header';
import Viewport from '../components/apply/viewport';
import Footer from '../components/footer';
import Form from '../components/apply/form';
import Flexibility from '../components/apply/flexibility';
import Pay from '../components/apply/pay';
import Review from '../components/apply/review';
import Start from '../components/apply/start';
import '../static/css/home.css';

class Apply extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'apply')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            return (
                <div>
                    <MetaTags>
                        <title> {RichText.asText(this.state.doc.data.title_tag)} </title>
                        <meta name="description" content={RichText.asText(this.state.doc.data.meta_description)} />
                    </MetaTags>
                    <Header/>
                    <Viewport/>
                    <Flexibility/>
                    <Pay/>
                    <Review/>
                    <Start/>
                    <Footer/>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Apply;
