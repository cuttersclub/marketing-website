import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Countdown from '../share/countdown';
import Upviral from '../share/upviral';

class Viewport extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'share')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const backgroundImage = { backgroundImage: "url(" + this.state.doc.data.image.url + ")" };
            return (
                <div style={backgroundImage} id="home_viewport" className="background-cover viewport-height-pc-553 vertical-center horizontal-center">
                    <div className="max-width-px-655 padding-top-361 padding-bottom-361 padding-left-166 padding-right-166">
                        <p className="color-738 margin-bottom-lh-166">{RichText.asText(this.state.doc.data.confirmation)}</p>
                        <h1 className="color-738 margin-bottom-lh-166">{RichText.asText(this.state.doc.data.title)}</h1>
                        <p className="color-738 margin-bottom-lh-428">{RichText.asText(this.state.doc.data.description_1)}</p>
                        <p className="color-738 margin-bottom-lh-428">{RichText.asText(this.state.doc.data.description_2)}</p>
                        <p className="color-738 margin-bottom-lh-166">{RichText.asText(this.state.doc.data.description_3)}</p>
                        <Countdown/>
                        <Upviral/>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Viewport;
