import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
    HashRouter,
} from 'react-router-dom';
import Home from './pages/home';
import Share from './pages/share';
import Apply from './pages/apply';
import Landing from './pages/landing';

import ApplyLondon from './pages/apply_london';

// custom css
import './static/css/base.css';
import './static/css/home.css';
import './static/css/custom/general.css';
import './static/css/custom/brand.css';
import './static/css/custom/margin.css';
import './static/css/custom/padding.css';
import './static/css/custom/height.css';
import './static/css/custom/width.css';
import './static/css/custom/position.css';
import './static/css/custom/font_size.css';
import './static/css/custom/font_weight.css';

const App = (props) => (
  <Router>
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/share' component={Share}/>
        <Route exact path='/apply' component={Apply}/>
        <Route exact path="/landing/:uid" render={routeProps => <Landing {...routeProps} prismicCtx={props.prismicCtx} />} />
        <Route exact path='/ad/apply/london' component={ApplyLondon}/>

        <Redirect from="/invest" to="/" />
        <Redirect from="/blog" to="/" />
        <Redirect from="/blog/londons-first-subscription-based-mobile-barber-service-is-coming" to="/" />

        <Redirect from="/barber-signup" to="/apply" />
        <Redirect from="/barber-sign-up" to="/apply" />

    </Switch>
  </Router>
);

export default App;
