import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import '../../static/css/pay.css';

class Pay extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'pay')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            return (
                <div className="background-color-191 horizontal-center">
                    <div className="max-width-px-257 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                        <h3 className="color-492 margin-bottom-lh-361">Earn more money.</h3>
                        <div id="earn-more-money-grid">
                            <div id="earn-more-money-grid-1" className="background-color-738 shadow border-radius-149 padding-top-361 padding-bottom-361 padding-left-166 padding-right-166">
                                <img className="height-px-361" src={this.state.doc.data.image_1.url}></img>
                                <h5 className="color-492 margin-top-lh-166 margin-bottom-lh-166">{RichText.asText(this.state.doc.data.title_1)}</h5>
                                <p className="color-366">{RichText.asText(this.state.doc.data.description_1)}</p>
                            </div>
                            <div id="earn-more-money-grid-2" className="background-color-738 shadow border-radius-149 padding-top-361 padding-bottom-361 padding-left-166 padding-right-166">
                                <img className="height-px-361" src={this.state.doc.data.image_2.url}></img>
                                <h5 className="color-492 margin-top-lh-166 margin-bottom-lh-166">{RichText.asText(this.state.doc.data.title_2)}</h5>
                                <p className="color-366">{RichText.asText(this.state.doc.data.description_2)}</p>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Pay;
